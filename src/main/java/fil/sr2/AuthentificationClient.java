package fil.sr2;

import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import fil.sr2.exception.AuthCredentialsException;

public class AuthentificationClient {
	private String username;
	private String password;
	private String token;

	public AuthentificationClient(String username, String password) {
		this.username = username;
		this.password = password;
	}

	/**
	 * Méthode qui permet de s'authentifier, et a comme effet de bord de sauvegarder
	 * le token
	 * 
	 * @param wb base uri
	 * @throws AuthCredentialsException excption si les identifiants sont faux
	 */
	public void auth(WebTarget wb) throws AuthCredentialsException {
		WebTarget connect = wb.path("login");
//		System.out.println(connect.toString());
		Form form = new Form();
		form.param("username", username).param("password", password);

		Response res = connect.request().accept(MediaType.APPLICATION_JSON)
				.post(Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED), Response.class);
		if (res.getStatus() != 200) {
			throw new AuthCredentialsException("username or password is incorrect");
		}
		this.token = res.readEntity(String.class);
	}

	public String getToken() {
		return token;
	}

}
