package fil.sr2;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.Set;
import java.util.concurrent.ExecutionException;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;

import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.json.simple.parser.ParseException;

import fil.sr2.exception.AuthCredentialsException;
import fil.sr2.exception.EmptyRepositoryException;
import fil.sr2.utils.InitLocalFolder;
import fil.sr2.watcher.DirectoryWatcherThread;
import fil.sr2.watcher.RemoteWatcherThread;

/**
 * Hello world!
 *
 */
public class App {
	private static Client client;
	private static WebTarget wb;
	private static AuthentificationClient ac;
	private static Set<String> allAlias;
	private static InitLocalFolder ilf = new InitLocalFolder();
	// public static Path localpath = Paths.get(System.getProperty("user.dir") +
	// "/data");;
	public static Path localpath;
	public static final String SEPARATOR = FileSystems.getDefault().getSeparator();

	/**
	 * @param path
	 */
	public static void createTarget(String path) {
		client = ClientBuilder.newClient().register(MultiPartFeature.class);
		wb = client.target(path);
	}

	/**
	 * @param location
	 * @throws ParseException
	 */
	public static void initAliasesFromServer(Path location) throws ParseException {
		Aliases a = new Aliases();
		allAlias = a.getAliases(wb, ac.getToken());
		a.createAllFolder(allAlias, location);
		System.out.println(allAlias);
	}

	/**
	 * @param location
	 * @param username
	 * @param password
	 * @throws AuthCredentialsException
	 * @throws ParseException
	 * @throws IOException
	 */
	public static void initialisation(Path location, String username, String password)
			throws AuthCredentialsException, ParseException, IOException {

		createTarget("http://127.0.0.1:8080/myapp");

		ac = new AuthentificationClient(username, password);
		ac.auth(wb);
		initAliasesFromServer(location);
		initContentForAllAlias(location);
	}

	public void auth(String username, String password) {

	}

	private static void initContentForOneAlias(String alias, Path path, String remotePath) throws IOException {
		InitLocalFolder ilf = new InitLocalFolder();
		ilf.downloadFolder(wb, ac.getToken(), alias, remotePath, path);
	}

	public static void initContentForAllAlias(Path p) throws IOException {

		for (String alias : allAlias) {
			initContentForOneAlias(alias, Paths.get(p.toString(), alias), "");
		}
	}

	private static void usage() {
		System.out.println("alias disponible : " + allAlias);
		System.out.println("Que voulez vous faire ? Tappez :");
		System.out.println("\t <ls> pour lister le contenu de la corbeille ");
		System.out.println("\t <rm> pour supprimer le contenu de la corbeille");
		System.out.println("\t <restore> pour récupérer le fichier ou dossier dans son emplacement initial");
		System.out.println("\t <quit> pour fermer le client");
	}

	private static void restore(String path,String alias) {
		
		
		ilf.relocate(wb, ac.getToken(), alias,"/.deleted"+ path, path);
	}

	private static void ls(String alias) throws EmptyRepositoryException {
		ilf.displayAllsubfolders(wb, ac.getToken(), alias, "/.deleted/");
	}

	private static void rm(String alias) {
		ilf.deleteFolder(wb, ac.getToken(), alias, "/.deleted");
		ilf.createDir(wb, "", alias, ac.getToken(), ".deleted");
	}

	
	
	
	public static void main(String[] args)
			throws InterruptedException, ExecutionException, ParseException, IOException {
		try (Scanner scanner = new Scanner(System.in)) {
			System.out.println("Bienvenue sur l'agent FlopBox :");
			while (true) {
				System.out.print("username : ");
				String username = scanner.nextLine().trim();
				System.out.print("password : ");
				String password = scanner.nextLine().trim();

				System.out.println("voulez vous choisir l'emplacement du téléchargement ?[O/n]");
				String choix = scanner.nextLine().trim().toLowerCase();
				switch (choix) {
				case "o":
					while (true) {
						choix = scanner.nextLine().trim();
						localpath = Paths.get(choix);
						if (Files.isDirectory(localpath)) {
							break;
						} else {
							System.out.println(localpath + " n'est pas un path valide");
						}
					}

					break;

				default:
					System.out.println("emplacement par defaut : " + System.getProperty("user.dir") + "/data");
					localpath = Paths.get(System.getProperty("user.dir") + "/data");
					break;
				}
				// localpath = Paths.get(System.getProperty("user.dir") + "/data");

				System.out.println("début du téléchargement des fichiers distants");
				try {
					initialisation(localpath, username, password);
					break;
				} catch (AuthCredentialsException e1) {
					System.out.println("username ou mot de passe incorrect");
				}
			}

			DirectoryWatcherThread t = new DirectoryWatcherThread(wb, ac.getToken(), allAlias, localpath);
			System.out.println("lancement du watcher local");
			t.lancement();
			System.out.println("lancement du watcher distant");

			RemoteWatcherThread t2 = new RemoteWatcherThread(wb, ac.getToken(), allAlias, "", localpath);
			t2.lancement();

			while (true) {
				usage();

				System.out.print("choix de l'alias : ");

				String alias = scanner.nextLine();
				if (!allAlias.contains(alias.trim())) {
					System.err.println("alias inconnue veuillez reessayer");
				} else {
					System.out.print("choix de l'action : ");
					String res = scanner.nextLine();
					switch (res) {
					case "ls":
						try {
							ls(alias);
						} catch (EmptyRepositoryException e) {

						}
						System.out.println("ls");
						break;
					case "rm":
						rm(alias);
						System.out.println("rm");
						break;
					case "restore":
						System.out.println("quel fichier/ dossier voulez vous sauvegarder ?");
						System.out.println("veuillez mettre le chemin d'accès vers le fichier");
						try {
							ls(alias);
						} catch (EmptyRepositoryException e) {
							// TODO Auto-generated catch block
							throw new RuntimeException();
						}
						String path = scanner.nextLine();
						restore(path,alias);
						System.out.println("restore");
						break;
					case "quit":
						System.exit(0);
					default:
						System.err.println("action inconnue");
						// usage();
					}
				}

				Thread.sleep(1000);

			}
		} catch (NoSuchElementException e) {
			System.out.println(" \n au revoir");
		}

	}
}
