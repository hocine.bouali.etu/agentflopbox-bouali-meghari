package fil.sr2.exception;

public class AuthCredentialsException extends Exception {
	/**
	 * Exception lorsque l'authentification à la plateforme FlopBox a echouer
	 */
	private static final long serialVersionUID = 1L;

	public AuthCredentialsException(String message) {
		super(message);
	}

}
