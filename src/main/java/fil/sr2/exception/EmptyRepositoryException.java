package fil.sr2.exception;

public class EmptyRepositoryException extends Exception {

	/**
	 * Exception lorsque le listing d'un serveur a échouer
	 */
	private static final long serialVersionUID = 1L;
	public EmptyRepositoryException(String message) {
		super(message);
	}

}
