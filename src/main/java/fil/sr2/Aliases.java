package fil.sr2;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Set;

import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * @author Bouali Hocine et Meghari Samy
 *
 */
public class Aliases {

	/**
	 * Méthode qui récupere les alias sur la plateforme FlopBox
	 * 
	 * @param wb    webtarget
	 * @param token le token
	 * @return les alias disponible
	 * @throws ParseException
	 */
	@SuppressWarnings("unchecked")
	public Set<String> getAliases(WebTarget wb, String token) throws ParseException {
		WebTarget alias = wb.path("serveur");
		Response res = alias.request().accept(MediaType.APPLICATION_JSON).header("Authorization", "Bearer " + token)
				.get(Response.class);
		if (res.getStatus() != 200) {
			System.out.println("echec");
			throw new RuntimeException("something went wrong");
		}
		JSONParser parser = new JSONParser();
		JSONObject al = (JSONObject) parser.parse(res.readEntity(String.class));
		return al.keySet();
	}

	/**
	 * Méthode qui permet de créer les dossiers des alias
	 * 
	 * @param aliases      tous les alias
	 * @param parentFolder le repertoire de destination
	 */
	public void createAllFolder(Set<String> aliases, Path parentFolder) {
		for (String alias : aliases) {

			try {
				createOneFolderForOneAlias(Paths.get(parentFolder.toString(), alias));
			} catch (IOException e) {
				// repository already exist , nothing to do
			}

		}
	}

	private void createOneFolderForOneAlias(Path path) throws IOException {
		Files.createDirectories(path);
	}

}
