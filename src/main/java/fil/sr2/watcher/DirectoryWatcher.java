package fil.sr2.watcher;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.client.WebTarget;

import fil.sr2.utils.InitLocalFolder;
import fil.sr2.utils.Utils;

/**
 * @author Bouali Hocine et Meghari Samy
 *
 */
public class DirectoryWatcher implements Runnable {
	private final WatchService watcher;
	private final Map<WatchKey, Path> keys;
	private Path path;
	private String token;
	private String alias;
	private WebTarget wb;

	/**
	 * Creates a WatchService and registers the given directory
	 */
	DirectoryWatcher(Path dir, String alias, String token, WebTarget wb) throws IOException {
		this.watcher = FileSystems.getDefault().newWatchService();
		this.keys = new HashMap<WatchKey, Path>();
		this.path = dir;
		this.token = token;
		this.alias = alias;
		this.wb = wb;
		walkAndRegisterDirectories(dir);
	}

	/**
	 * Register the given directory with the WatchService; This function will be
	 * called by FileVisitor
	 */
	private void registerDirectory(Path dir) throws IOException {
		WatchKey key = dir.register(watcher, StandardWatchEventKinds.ENTRY_CREATE, StandardWatchEventKinds.ENTRY_DELETE,
				StandardWatchEventKinds.ENTRY_MODIFY);
		keys.put(key, dir);
	}

	/**
	 * Register the given directory, and all its sub-directories, with the
	 * WatchService.
	 */
	private void walkAndRegisterDirectories(final Path start) throws IOException {
		// register directory and sub-directories
		Files.walkFileTree(start, new SimpleFileVisitor<Path>() {
			@Override
			public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
				registerDirectory(dir);
				return FileVisitResult.CONTINUE;
			}
		});
	}

	/**
	 * Process all events for keys queued to the watcher
	 */
	void processEvents() {
		for (;;) {

			// wait for key to be signalled
			WatchKey key;
			try {
				key = watcher.take();
			} catch (InterruptedException x) {
				return;
			}

			Path dir = keys.get(key);
			if (dir == null) {
				System.err.println("WatchKey not recognized!!");
				continue;
			}

			for (WatchEvent<?> event : key.pollEvents()) {
				@SuppressWarnings("rawtypes")
				WatchEvent.Kind kind = event.kind();

				// Context for directory entry event is the file name of entry
				@SuppressWarnings("unchecked")
				Path name = ((WatchEvent<Path>) event).context();
				Path child = dir.resolve(name);

				// print out event
				// System.out.format("%s: %s\n", event.kind().name(), child);

				serviceFTP(kind, child);
				// if directory is created, and watching recursively, then register it and its
				// sub-directories
				if (kind == StandardWatchEventKinds.ENTRY_CREATE) {
					try {
						if (Files.isDirectory(child)) {
							walkAndRegisterDirectories(child);
						}
					} catch (IOException x) {
						// do something useful
					}
				}
			}

			// reset key and remove from set if directory no longer accessible
			boolean valid = key.reset();
			if (!valid) {
				keys.remove(key);

				// all directories are inaccessible
				if (keys.isEmpty()) {
					break;
				}
			}
		}
	}

	/**
	 * Méthode qui va en fonction des evenements upload ou supprimer ou creer un
	 * repertoire sur le serveur distant
	 * 
	 * @param kind  l'evenement produit (CREATE ,DELETE ,MODIFY)
	 * @param child le fichier/dossier qui a subit un changement
	 */
	@SuppressWarnings("rawtypes")
	private void serviceFTP(WatchEvent.Kind kind, Path child) {
		InitLocalFolder ilf = new InitLocalFolder();
		if (kind == StandardWatchEventKinds.ENTRY_MODIFY) {
			if (Files.exists(child)) {
				if (!Files.isDirectory(child)) {
					ilf.uploadFile(wb, token, alias, child.toString(), Utils.getParentPathname(child));
				}
			} else {
				ilf.deleteFolder(wb, token, alias, Utils.getAllPathname(child));
			}

		} else if (kind == StandardWatchEventKinds.ENTRY_DELETE) {

			ilf.createDirs(wb, Utils.getParentPathname(child), alias, token, ".deleted/");
			ilf.relocate(wb, token, alias, Utils.getAllPathname(child), "/.deleted/" + Utils.getAllPathname(child));

			if (Files.notExists(child)) {
				ilf.deleteFolder(wb, token, alias, Utils.getAllPathname(child));
				ilf.createDirs(wb, Utils.getParentPathname(child), alias, token, ".deleted/");
				ilf.relocate(wb, token, alias, Utils.getAllPathname(child), "/.deleted/" + Utils.getAllPathname(child));
			}
//			}
		} else if (kind == StandardWatchEventKinds.ENTRY_CREATE) {
			if (Files.isDirectory(child)) {
				ilf.createDir(wb, Utils.getParentPathname(child), alias, token, child.getFileName().toString());
			} else {

				ilf.uploadFile(wb, token, alias, child.toString(), Utils.getParentPathname(child));
			}

		}
	}

	@Override
	public void run() {
		try {
			new DirectoryWatcher(path, alias, token, wb).processEvents();
		} catch (IOException e) {
			throw new RuntimeException(e.getMessage());
		}
	}
}
