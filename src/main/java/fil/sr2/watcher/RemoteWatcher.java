package fil.sr2.watcher;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.client.WebTarget;

import fil.sr2.utils.Folder;
import fil.sr2.utils.InitLocalFolder;
import fil.sr2.utils.Utils;

/**
 * @author Bouali Hocine et Meghari Samy
 *
 */
public class RemoteWatcher implements Runnable {

	private InitLocalFolder ilf = new InitLocalFolder();
	private Path localPath;
	private String token;
	private String alias;
	private WebTarget wb;
	private String remotePath;

	public RemoteWatcher(WebTarget wb, String token, String alias, String remotePath, Path localPath) {
		this.localPath = localPath;
		this.token = token;
		this.alias = alias;
		this.wb = wb;
		this.remotePath = remotePath;
	}

	/**
	 * Méthode qui permet de supprimer les fichiers locaux qui ne plus sur le
	 * serveur
	 * 
	 * @param list      les fichiers distants
	 * @param localpath path local vers le dossier à traiter
	 */
	private void deleteInLocal(List<Folder> list, Path localpath) {
		try {
			List<Folder> localFolders = new ArrayList<Folder>();

			Files.list(localpath).forEach(
					path -> localFolders.add(new Folder(path.getFileName().toString(), Files.isDirectory(path))));
			for (Folder folder : localFolders) {
				if (!list.contains(folder)) {
					Path p = Paths.get(localpath.toString() + "/" + folder.getName());
					if (Files.isDirectory(p)) {
						Files.walkFileTree(p, new SimpleFileVisitor<Path>() {
							@Override
							public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
								Files.delete(dir);
								return FileVisitResult.CONTINUE;
							}

							@Override
							public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
								Files.delete(file);
								return FileVisitResult.CONTINUE;
							}
						});
					} else {
						Files.delete(p);
					}

				}
			}
		} catch (IOException e) {
			throw new RuntimeException();
		}
	}

	/**
	 * Méthode qui parcourt de maniere recursive un serveur FTP et va comparer les
	 * fichiers entre eux pour decider si ils doivent etre uploader ou downloader
	 * 
	 * @param wb         webtarget
	 * @param token      token de connexion pour FlopBox
	 * @param alias      un alias
	 * @param remotePath le lien distant
	 * @param localPath  le lien local
	 * @throws IOException si un téléchargement d'un fichier a échouer
	 */
	public void browse(WebTarget wb, String token, String alias, String remotePath, Path localPath) throws IOException {
		Files.createDirectories(localPath);
		List<Folder> remoteFiles = new ArrayList<Folder>();
		try {
			remoteFiles = Utils.parse(ilf.listing(wb, token, alias, remotePath));
		} catch (Exception e) {

			return;
		} finally {
			deleteInLocal(remoteFiles, localPath);
		}
		for (Folder remoteFile : remoteFiles) {
			if(!remoteFile.isLink()) {
				String remoteFilePath = remotePath + "/" + remoteFile.getName();
				String localFilePath = localPath + "/" + remoteFile.getName();
				if (remoteFile.isDir()) {

					Path p = Paths.get(localFilePath);
					if (!Files.isDirectory(p)) {
						Files.createDirectories(p);
					}
					browse(wb, token, alias, remoteFilePath, Paths.get(localFilePath));
				} else {
					Path p = null;
					FileTime remoteTime = Utils.timeConverter(remoteFile.getDateModif(), "yyyy-MM-dd HH:mm:ss");

					p = Paths.get(localPath + "/" + remoteFile.getName());
					if (!Files.exists(p)) {
						ilf.downloadFile(wb, token, alias, remoteFilePath, localPath);
						Files.setLastModifiedTime(p, remoteTime);
						continue;
					}

					BasicFileAttributes attr = Files.readAttributes(p, BasicFileAttributes.class);
					FileTime localTime = FileTime.fromMillis(attr.lastModifiedTime().toMillis());

					if (remoteTime.compareTo(localTime) == 0) {
						continue;
					} else if (remoteTime.compareTo(localTime) < 0) {
						ilf.uploadFile(wb, token, alias, localFilePath, remotePath);

					} else if (remoteTime.compareTo(localTime) > 0) {
						ilf.downloadFile(wb, token, alias, remoteFilePath, localPath);
						Files.setLastModifiedTime(p, remoteTime);
					}
				}
			}
			
		}
	}

	@Override
	public void run() {
		try {

			while (true) {
				new RemoteWatcher(wb, token, alias, remotePath, localPath).browse(wb, token, alias, remotePath,
						localPath);
				Thread.sleep(30000);
			}

		} catch (IOException | InterruptedException e) {

		}

	}
}
