package fil.sr2.watcher;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Set;

import javax.ws.rs.client.WebTarget;

/**
 * Classe qui permet le lancement des thread pour observer les differents alias
 * en local
 * 
 * @author Bouali Hocine et Meghari Samy
 *
 */
public class DirectoryWatcherThread {
	private WebTarget wb;
	private String token;
	private Set<String> aliases;
	private Path path;

	public DirectoryWatcherThread(WebTarget wb, String token, Set<String> aliases, Path path) {

		this.aliases = aliases;
		this.wb = wb;
		this.token = token;
		this.path = path;
	}

	/**
	 * Lance les threads sur les differents serveur FTP(alias)
	 * 
	 * @throws IOException
	 */
	public void lancement() throws IOException {
		for (String alias : aliases) {
			DirectoryWatcher dw = new DirectoryWatcher(Paths.get(path.toString(), alias), alias, token, wb);
			Thread thread = new Thread(dw);
			thread.start();
		}
	}

}
