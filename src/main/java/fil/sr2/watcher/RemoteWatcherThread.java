package fil.sr2.watcher;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Set;

import javax.ws.rs.client.WebTarget;

/**
 * Classe qui permet le lancement des thread pour observer les differents
 * Serveur FTP
 * 
 * @author Bouali Hocine et Meghari Samy
 *
 */
public class RemoteWatcherThread {
	private WebTarget wb;
	private String token;
	private Set<String> aliases;
	private String remotePath;
	private Path localpath;

	public RemoteWatcherThread(WebTarget wb, String token, Set<String> aliases, String remotePath, Path localpath) {

		this.aliases = aliases;
		this.wb = wb;
		this.token = token;
		this.remotePath = remotePath;
		this.localpath = localpath;
	}

	/**
	 * Lance les threads sur les differents serveur FTP(alias)
	 * 
	 * @throws IOException
	 */
	public void lancement() throws IOException {
		for (String alias : aliases) {
			RemoteWatcher dw = new RemoteWatcher(wb, token, alias, remotePath, Paths.get(localpath.toString(), alias));
			Thread thread = new Thread(dw);
			thread.start();
		}
	}
}
