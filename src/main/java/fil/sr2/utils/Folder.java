package fil.sr2.utils;

/**
 * @author Bouali Hocine et Meghari Samy
 *
 */
public class Folder {
	private String dateModif;
	private boolean isDir;
	private String name;
	private int size;
	private boolean isLink;

	
	public Folder(String name, boolean isDir) {
		this.name = name;
		this.isDir = isDir;
	}

	public Folder() {

	}

	public String getDateModif() {
		return dateModif;
	}

	public void setDateModif(String dateModif) {
		this.dateModif = dateModif;
	}

	public boolean isDir() {
		return isDir;
	}

	public void setDir(boolean isDir) {
		this.isDir = isDir;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}
	public boolean isLink() {
		return isLink;
	}

	public void setLink(boolean isLink) {
		this.isLink = isLink;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof Folder)) {
			return false;
		}
		Folder f = (Folder) obj;
		if (!name.equals(f.name)) {
			return false;
		}
		return isDir == f.isDir;
	}

	@Override
	public String toString() {
		return getName() + " " + getSize() + " " + getDateModif();
	}

}
