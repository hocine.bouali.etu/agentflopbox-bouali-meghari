package fil.sr2.utils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.ResourceBundle;

import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.media.multipart.MultiPart;
import org.glassfish.jersey.media.multipart.file.FileDataBodyPart;

import fil.sr2.exception.EmptyRepositoryException;

public class InitLocalFolder {

	/**
	 * Méthode qui permet de recuperer la date de derneire modification d'un fichier
	 * distant
	 * 
	 * @param wb    webtarget
	 * @param token le token
	 * @param alias l'alias
	 * @param path  le path distant du fichier
	 * @return la date
	 */
	public String lastModifiedDate(WebTarget wb, String token, String alias, String path) {
		ResourceBundle bundle = ResourceBundle.getBundle("fil.sr2.properties.config");
		String username = bundle.getString(alias + ".username");
		String password = bundle.getString(alias + ".password");
		String mode = bundle.getString(alias + ".mode");
		String type = bundle.getString(alias + ".type");
		WebTarget lastmodified = wb.path("ftp/" + alias + "/file/last-modified" + path);
		Response res = lastmodified.request().accept(MediaType.TEXT_PLAIN).header("Authorization", "Bearer " + token)
				.header("username", username).header("password", password).header("mode", mode).header("type", type)
				.get(Response.class);
		if (res.getStatus() != 200) {
			throw new RuntimeException("something went wrong in retrieving the last modification date ");
		}
		return res.readEntity(String.class);
	}

	/**
	 * méthode qui permet d'afficher le contenu entier d'un reperoire ( ainsi que
	 * les sous repertoires)
	 * 
	 * @param wb         webtarget
	 * @param token      le token
	 * @param alias      l'alias
	 * @param remotePath le réperoire
	 * @throws EmptyRepositoryException le répertoire est vide
	 */
	public void displayAllsubfolders(WebTarget wb, String token, String alias, String remotePath)
			throws EmptyRepositoryException {
		List<Folder> remoteFiles;
		try {
			remoteFiles = Utils.parse(listing(wb, token, alias, remotePath));
		} catch (Exception e) {
			System.out.println("le dossier /.deleted/ est vide ");
			throw new EmptyRepositoryException("le dossier /.deleted/ est vide ");
		}
		if (remotePath == "") {
			System.out.println("/");
		} else {
			System.out.println(remotePath);
		}

		for (Folder remoteFile : remoteFiles) {
			String remoteFilePath = remotePath + "/" + remoteFile.getName();

			if (remoteFile.isDir()) {
				System.out.println("│    └── " + remoteFile.getName());
				afficheUnDossier(wb, token, alias, remoteFilePath, "");

			} else {
				System.out.println("│    └── " + remoteFile.getName());

			}
		}
	}

	private void afficheUnDossier(WebTarget wb, String token, String alias, String remotePath, String space) {
		List<Folder> remoteFiles;
		try {
			remoteFiles = Utils.parse(listing(wb, token, alias, remotePath));
		} catch (Exception e) {
			return;
		}
		space += "│    ";
		for (Folder remoteFile : remoteFiles) {
			if (remoteFile.isDir()) {
				System.out.println(space + "│    └── " + remoteFile.getName());
				afficheUnDossier(wb, token, alias, remotePath + "/" + remoteFile.getName(), space);
			} else {
				System.out.println(space + "│    └── " + remoteFile.getName());
			}
		}
	}

	/**
	 * méthode qui permet de lister un repertoire distant
	 * 
	 * @param wb    webtarget
	 * @param token le token
	 * @param alias l'alias
	 * @param path  le repertoire
	 * @return renvoie le listing du repertoire
	 */
	public String[] listing(WebTarget wb, String token, String alias, String path) {
		ResourceBundle bundle = ResourceBundle.getBundle("fil.sr2.properties.config");

		String username = bundle.getString(alias + ".username");
		String password = bundle.getString(alias + ".password");
		String mode = bundle.getString(alias + ".mode");
		String type = bundle.getString(alias + ".type");
		if (path.isEmpty()) {
			path = "/";
		}
		WebTarget list = wb.path("ftp/" + alias + "/list" + path);
		Response res = list.request().accept(MediaType.TEXT_PLAIN).header("Authorization", "Bearer " + token)
				.header("username", username).header("password", password).header("mode", mode).header("type", type)
				.get(Response.class);
		if (res.getStatus() != 200) {
			throw new RuntimeException("something went wrong while listing the repository");
		}
		String tmp = res.readEntity(String.class);
		if(tmp.isEmpty()) {
			return new String[0];
		}

		return tmp.split("\n");
	}

	/**
	 * Méthode qui permet d'uplaod un fichier
	 * 
	 * @param wb         webtarget
	 * @param token      le token
	 * @param alias      l'alias
	 * @param localPath  le path local du ficher a upload
	 * @param remotePath le path de destination
	 */
	public void uploadFile(WebTarget wb, String token, String alias, String localPath, String remotePath) {
		ResourceBundle bundle = ResourceBundle.getBundle("fil.sr2.properties.config");
		String username = bundle.getString(alias + ".username");
		String password = bundle.getString(alias + ".password");
		String mode = bundle.getString(alias + ".mode");
		String type = bundle.getString(alias + ".type");
		WebTarget upload = wb.path("ftp/" + alias + "/file/" + remotePath);
		MultiPart multiPart = new MultiPart();
		multiPart.setMediaType(MediaType.MULTIPART_FORM_DATA_TYPE);

		FileDataBodyPart fileDataBodyPart = new FileDataBodyPart("file", new File(localPath),
				MediaType.APPLICATION_OCTET_STREAM_TYPE);
		multiPart.bodyPart(fileDataBodyPart);

		upload.request().header("Authorization", "Bearer " + token).header("username", username)
				.header("password", password).header("type", type).header("mode", mode)
				.post(Entity.entity(multiPart, multiPart.getMediaType()));

	}

	/**
	 * méthode qui permet de supprimer un fichier distant
	 * 
	 * @param wb         webtarget
	 * @param token      le token
	 * @param alias      l'alias
	 * @param remotePath le path du fichier à supprimer
	 */
	public void deleteFile(WebTarget wb, String token, String alias, String remotePath) {
		ResourceBundle bundle = ResourceBundle.getBundle("fil.sr2.properties.config");
		String username = bundle.getString(alias + ".username");
		String password = bundle.getString(alias + ".password");
		String mode = bundle.getString(alias + ".mode");
		String type = bundle.getString(alias + ".type");
		WebTarget upload = wb.path("ftp/" + alias + "/file/" + remotePath);
		upload.request().header("Authorization", "Bearer " + token).header("username", username).header("mode", mode)
				.header("password", password).header("type", type).delete();

	}

	/**
	 * Méthode qui permet de supprimer un répertoire
	 * 
	 * @param wb         webtarget
	 * @param token      le token
	 * @param alias      l'alias
	 * @param remotePath le lien du dossier à supprimer
	 */
	public void deleteFolder(WebTarget wb, String token, String alias, String remotePath) {
		ResourceBundle bundle = ResourceBundle.getBundle("fil.sr2.properties.config");
		String username = bundle.getString(alias + ".username");
		String password = bundle.getString(alias + ".password");
		String mode = bundle.getString(alias + ".mode");
		String type = bundle.getString(alias + ".type");
		WebTarget upload = wb.path("ftp/" + alias + "/dir/" + remotePath);
		upload.request().header("Authorization", "Bearer " + token).header("username", username).header("mode", mode)
				.header("password", password).header("type", type).delete();

	}

	/**
	 * méthode qui permet de creer un repertoire sur le serveur distant
	 * 
	 * @param wb         webtarget
	 * @param token      le token
	 * @param alias      l'alias
	 * @param remotePath l'emplacement du futur dossier
	 */
	public void createFolder(WebTarget wb, String token, String alias, String remotePath) {
		ResourceBundle bundle = ResourceBundle.getBundle("fil.sr2.properties.config");
		String username = bundle.getString(alias + ".username");
		String password = bundle.getString(alias + ".password");
		String mode = bundle.getString(alias + ".mode");
		String type = bundle.getString(alias + ".type");
		WebTarget upload = wb.path("ftp/" + alias + "/new-dir/" + remotePath);
		upload.request().header("Authorization", "Bearer " + token).header("username", username).header("mode", mode)
				.header("password", password).header("type", type).post(Entity.entity("", MediaType.TEXT_PLAIN));
	}

	/**
	 * Méthode qui permet de déplacer un fichier ou un dossier sur le serveur
	 * 
	 * @param wb            webtarget
	 * @param token         token
	 * @param alias         l'alias
	 * @param oldRemotePath le path initial
	 * @param remotePath    le path de destionation
	 */
	public void relocate(WebTarget wb, String token, String alias, String oldRemotePath, String remotePath) {

		ResourceBundle bundle = ResourceBundle.getBundle("fil.sr2.properties.config");
		String username = bundle.getString(alias + ".username");
		String password = bundle.getString(alias + ".password");
		String mode = bundle.getString(alias + ".mode");
		String type = bundle.getString(alias + ".type");
		WebTarget upload = wb.path("ftp/" + alias + "/relocate/" + oldRemotePath);
		Response res = upload.queryParam("to", remotePath).request().header("Authorization", "Bearer " + token).header("mode", mode)
				.header("username", username).header("password", password).header("type", type)
				.put(Entity.entity("", MediaType.TEXT_PLAIN));
		System.out.println(res.getStatus()+ " "+ res.getStatusInfo()+" "+res);

	}

	/**
	 * Méthode qui permet de télécharger un fichier
	 * 
	 * @param wb        webtarget
	 * @param token     token
	 * @param alias     l'alias
	 * @param path      le path distant du fichier
	 * @param localPath le path de destination
	 * @return le nom du fichier
	 * @throws IOException path incorrect
	 */
	public String downloadFile(WebTarget wb, String token, String alias, String path, Path localPath)
			throws IOException {
		System.out.println(path);
		ResourceBundle bundle = ResourceBundle.getBundle("fil.sr2.properties.config");
		String username = bundle.getString(alias + ".username");
		String password = bundle.getString(alias + ".password");
		String mode = bundle.getString(alias + ".mode");
		String type = bundle.getString(alias + ".type");
		WebTarget down = wb.path("ftp/" + alias + "/file" + path);
		Response res = down.request().accept(MediaType.APPLICATION_OCTET_STREAM)
				.header("Authorization", "Bearer " + token).header("username", username).header("password", password)
				.header("mode", mode).header("type", type).get(Response.class);

		if (res.getStatus() != 200) {
			throw new IOException("failed to download the file");
		}
		InputStream in = res.readEntity(InputStream.class);

		String filename = path.substring(path.lastIndexOf("/") + 1);
		Path p = Paths.get(localPath.toString(), filename);
		Files.copy(in, p, StandardCopyOption.REPLACE_EXISTING);
		return filename;
	}

	/**
	 * Méthode qui permet de creer un repertoire distant
	 * 
	 * @param wb         webtarget
	 * @param parentPath la path parent de destination
	 * @param alias      l'alias
	 * @param token      le token
	 * @param name       le nom du dossier à creer
	 */
	public void createDir(WebTarget wb, String parentPath, String alias, String token, String name) {
		ResourceBundle bundle = ResourceBundle.getBundle("fil.sr2.properties.config");
		String username = bundle.getString(alias + ".username");
		String password = bundle.getString(alias + ".password");
		String mode = bundle.getString(alias + ".mode");
		String type = bundle.getString(alias + ".type");
		WebTarget down = wb.path("ftp/" + alias + "/new-dir/" + parentPath);
		down.queryParam("name", name).request().header("Authorization", "Bearer " + token).header("mode", mode)
				.header("username", username).header("password", password).header("type", type)
				.post(Entity.entity("", MediaType.TEXT_PLAIN));

	}

	/**
	 * Méthode qui creer tous les répertoire parents d'un elements
	 * 
	 * @param wb         webtarget
	 * @param parentPath le path parent
	 * @param alias      l'alias
	 * @param token      le token
	 * @param dest       le path de destination
	 */
	public void createDirs(WebTarget wb, String parentPath, String alias, String token, String dest) {
		String[] tmp = parentPath.split("/");
		String newpath = dest;
		for (String s : tmp) {
			createDir(wb, newpath, alias, token, s);
			newpath += "/" + s;

		}
	}

	/**
	 * Méthode qui permet de télécharger un répertoire entier
	 * 
	 * @param wb         webtarget
	 * @param token      le token
	 * @param alias      l'alias
	 * @param remotePath le path du dossier à télécharger
	 * @param localPath  le path local de destination
	 * @throws IOException
	 */
	public void downloadFolder(WebTarget wb, String token, String alias, String remotePath, Path localPath)
			throws IOException {
		List<Folder> remoteFiles;
		try {
			remoteFiles = Utils.parse(listing(wb, token, alias, remotePath));
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}
		for (Folder remoteFile : remoteFiles) {
			if(!remoteFile.isLink()) {
				String remoteFilePath = remotePath + "/" + remoteFile.getName();

				if (remoteFile.isDir()) {
					Path p = Paths.get(localPath.toString(), remoteFile.getName());
					Files.createDirectories(p);
					downloadFolder(wb, token, alias, remoteFilePath, p);
					Files.setLastModifiedTime(p, Utils.timeConverter(remoteFile.getDateModif(), "yyyy-MM-dd HH:mm:ss"));
				} else {
					try {
						String filename = downloadFile(wb, token, alias, remoteFilePath, localPath);
						Path p = Paths.get(localPath.toString(), filename);
						Files.setLastModifiedTime(p, Utils.timeConverter(remoteFile.getDateModif(), "yyyy-MM-dd HH:mm:ss"));
					} catch (IOException e) {
						return;
					}

				}
			}
			
		}
	}

}
