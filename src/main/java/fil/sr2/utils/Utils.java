package fil.sr2.utils;

import java.nio.file.Path;
import java.nio.file.attribute.FileTime;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import fil.sr2.App;

/**
 * @author Bouali Hocine et Meghari Samy
 *
 */

public class Utils {

	/**
	 * Méthode qui permet de convertir une date en un FileTime
	 * 
	 * @param date la date à convertir
	 * @param type le format de conversion
	 * @return
	 */
	public static FileTime timeConverter(String date, String type) {
		Date d;
		try {
			d = new SimpleDateFormat(type).parse(date);
			FileTime time = FileTime.fromMillis(d.getTime());
			return time;
		} catch (ParseException e) {
			return null;
		}

	}

	/**
	 * méthode qui permet de parser la sortie du listing pour les transformer en
	 * Objet Folder
	 * 
	 * @param file un tableau de file qui correpond au retour du listing d'u
	 *             repertoire distant
	 * @return liste de Folder
	 */
	public static List<Folder> parse(String[] file) {

		List<Folder> listfiles = new ArrayList<Folder>();
		for (String f : file) {
			listfiles.add(parseFile(f, String.valueOf(f.trim().charAt(0))));
		}
		return listfiles;

	}

	private static Folder parseFile(String file, String begin) {
		// System.err.println(begin + file);
		Folder f = new Folder();
		if (begin.equals("/")) {
			f.setLink(true);
			return f;
		}
		String tmp = "";
		if (begin.equals("<")) {
			tmp = ">";
		} else if (begin.equals("[")) {
			tmp = "]";
		}

		int index = file.indexOf(tmp);
		String name = file.substring(1, index);
		String endLine = file.substring(index + 1).trim();
		endLine = endLine.replaceAll("(\\s)+", " ");
		String[] parse = endLine.split(" ");
		f.setSize(Integer.parseInt(parse[0]));
		f.setDateModif(parse[2] + " " + parse[3]);
		if (begin.equals("<")) {
			f.setDir(false);
		} else if (begin.equals("[")) {
			f.setDir(true);
		}

		f.setName(name);

		return f;
	}

	public static String getFileName(String content_disp) {
		String prefix = "filename=\"";
		content_disp = content_disp.substring(content_disp.indexOf(prefix) + prefix.length(), content_disp.length());
		String filename = content_disp.substring(0, content_disp.indexOf("\""));
		return filename;
	}

	/**
	 * Méthode qui permet de convertir un path local en un path distant
	 * 
	 * @param path le path local
	 * @return le path equivalent distant
	 */
	public static String getAllPathname(Path path) {
		int indexDebut = App.localpath.getNameCount() + 1;// +1 pour l'alias
		int indexFin = path.getNameCount(); // pour avoir le parent
		if (indexDebut == indexFin) {
			return "";
		}
		String res = path.subpath(indexDebut, indexFin).toString();
		if (App.SEPARATOR.equals("\\")) {
			res = res.replace("\\", "/");
		}
		return res;
	}

	/**
	 * Méthode qui permet de convertir un path local en un path parent(sans le
	 * dernier element du path local) valide sur le serveur
	 * 
	 * @param path un path local
	 * @return l'equivalent du path sur le serveur
	 */
	public static String getParentPathname(Path path) {
		int indexDebut = App.localpath.getNameCount() + 1;// +1 pour l'alias
		int indexFin = path.getNameCount() - 1; // pour avoir le parent
		if (indexDebut == indexFin) {
			return "";
		}
		String res = path.subpath(indexDebut, indexFin).toString();
		if (App.SEPARATOR.equals("\\")) {
			res = res.replace("\\", "/");
		}
		return res;
	}

}
