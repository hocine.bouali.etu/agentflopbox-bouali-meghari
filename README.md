## Implementation d'un Agent <FlopBox>

 Bouali Hocine 
 
 Meghari Samy  
  		
 29/04/2021

#### Introduction

Ce projet permet de synchroniser les données stockées à distance sur une multitude de serveurs FTP avec le système de fichier local d'une machine sur laquelle l'application cliente sera exécutée.

L'application va se connecter à la plateforme FlopBox avec un compte _INITIALEMENT_ crée, récupérer la liste des alias  _préalablement_ initialisés
puis créer un répertoire pour chaque alias et y stocker tous les fichiers du serveur FTP sous-jacent pour lesquels l'utilisateur a un accès (les clés d'accès aux serveurs FTP seront transmis à l'application cliente via un fichier de configuration)



#### Exigences

L'ensemble des exigences ont été respectées .(le client ne supporte pas les liens symboliques)

## Utilisation 


##### Etape 1: récupérer le projet


Récuperer et lancer FlopBox

	$git clone https://gitlab.univ-lille.fr/hocine.bouali.etu/flopbox-bouali.git
	
Vous devez obligatoirement le lancer et créer au moins un alias et un utilisateur FlopBox (vos identifiants vous permettront de vous connecter à l'Agent FlopBox )


 Clone du dépôt dans le répertoire de votre choix

	$git clone https://gitlab.univ-lille.fr/hocine.bouali.etu/agentflopbox-bouali-meghari.git
 
 Placez-vous dans le bon dossier

	$cd agentflopbox-bouali-meghari/
	
	
N'OUBLIEZ PAS D'EDITER LE FICHIER DE CONFIGURATION (src/main/java/fil/sr2/properties/config.properties)

#### le fichier de configuration

Le fichier de configuration est à editer dans le package _fil.sr2.config.properties_ sous le format 

	 alias.username=username
	 alias.password=password
	 alias.mode=[port/pasv]
	 alias.type=[FTP/FTPS]

(alias est à remplacer par l'alias que vous avez préalablement créer)

il est OBLIGATOIRE de renseigner les 4 champs 

* type : FTP ou FTPS
* mode : pasv ou port 

##### Etape 2: exécuter le projet

 Compilation du projet 
 
	$mvn package assembly:single
	
 Exécution du projet:
 
	$java -jar target/agent-flopbox-1.0-SNAPSHOT-jar-with-dependencies.jar
	


	
# Architecture



##### Gestion d'erreur

Si la réponse de l'API n'est pas correcte, une exception est levée lors de l'authentification à la plateforme FlopBox

	public void auth(WebTarget wb) throws AuthCredentialsException {
		...

		Response res = connect.request().accept(MediaType.APPLICATION_JSON)
				.post(Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED), Response.class);
		if (res.getStatus() != 200) {
			throw new AuthCredentialsException("username or password is incorrect");
		}
		this.token = res.readEntity(String.class);
	}
	
	
Si la réponse de l'API n'est pas correcte, une exception est levée lors du listing d'un repertoire sur un serveur FTP

	public String[] listing(WebTarget wb, String token, String alias, String path) {
		...
		WebTarget list = wb.path("ftp/" + alias + "/list" + path);
		Response res = list.request().accept(MediaType.TEXT_PLAIN).header("Authorization", "Bearer " + token)
				.header("username", username).header("password", password).header("mode", mode).header("type", type)
				.get(Response.class);
		if (res.getStatus() != 200) {
			throw new RuntimeException("something went wrong while listing the repository");
		}
		return res.readEntity(String.class).split("\n");
		
		
		
# Code Samples


Méthode qui permet d'upload un fichier sur un serveur FTP

	public void uploadFile(WebTarget wb, String token, String alias, String localPath, String remotePath) {
		...
		WebTarget upload = wb.path("ftp/" + alias + "/file/" + remotePath);
		MultiPart multiPart = new MultiPart();
		multiPart.setMediaType(MediaType.MULTIPART_FORM_DATA_TYPE);

		FileDataBodyPart fileDataBodyPart = new FileDataBodyPart("file", new File(localPath),
				MediaType.APPLICATION_OCTET_STREAM_TYPE);
		multiPart.bodyPart(fileDataBodyPart);

		Response response = upload.request().header("Authorization", "Bearer " + token).header("username", username)
				.header("password", password).header("type", type)
				.post(Entity.entity(multiPart, multiPart.getMediaType()));


	}


Méthode qui permet de synchroniser le coté serveur->local, si la date est supérieure le fichier est télécharger, si la date est inférieure le fichier est uploadé, si c'est la même date, la méthode ne fait rien

	public void browse(WebTarget wb, String token, String alias, String remotePath, Path localPath) throws IOException {
		...
		for (Folder remoteFile : remoteFiles) {
			...
			if (remoteFile.isDir()) {

				Path p = Paths.get(localFilePath);
				if (!Files.isDirectory(p)) {
					Files.createDirectories(p);
				}
				browse(wb, token, alias, remoteFilePath, Paths.get(localFilePath));
			} else {
				Path p = null;
				FileTime remoteTime = Utils.timeConverter(remoteFile.getDateModif(), "yyyy-MM-dd HH:mm:ss");

				p = Paths.get(localPath + "/" + remoteFile.getName());
				if (!Files.exists(p)) {
					ilf.downloadFile(wb, token, alias, remoteFilePath, localPath);
					Files.setLastModifiedTime(p, remoteTime);
					continue;
				}

				BasicFileAttributes attr = Files.readAttributes(p, BasicFileAttributes.class);
				FileTime localTime = FileTime.fromMillis(attr.lastModifiedTime().toMillis());

				if (remoteTime.compareTo(localTime) == 0) {
					continue;
				} else if (remoteTime.compareTo(localTime) < 0) {
					ilf.uploadFile(wb, token, alias, localFilePath, remotePath);

				} else if (remoteTime.compareTo(localTime) > 0) {
					ilf.downloadFile(wb, token, alias, remoteFilePath, localPath);
					Files.setLastModifiedTime(p, remoteTime);
				}
			}

